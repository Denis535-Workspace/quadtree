package com.demo.frame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author DDDENISSS
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Parent page = (Parent) FXMLLoader.load(Main.class.getResource("Frame.fxml"));
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.setTitle("Voxel Tracing");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
