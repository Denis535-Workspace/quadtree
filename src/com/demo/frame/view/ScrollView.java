/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.frame.view;

import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.transform.Affine;

/**
 *
 * @author DDDENISSS
 */
public abstract class ScrollView extends Canvas {
    
    protected final Affine matrix = new Affine();
    protected Point2D mouse = null;
    
    public ScrollView() {
        widthProperty().addListener( (o, oldW, newW) -> paint() );
        heightProperty().addListener( (o, oldH, newH) -> paint() );
        
        setOnMousePressed( this::onMousePressed );
        setOnMouseDragged( this::onMouseDragged );
        setOnScroll( this::onScroll );
    }
    
    protected abstract void paint();
    
    protected void onMousePressed(MouseEvent evt) {
        mouse = new Point2D( evt.getX(), evt.getY() );
        if (evt.isShiftDown()) {
            evt.consume();
        }
    }
 
    protected void onMouseDragged(MouseEvent evt) {
        Point2D newMouse = new Point2D( evt.getX(), evt.getY() );
        Point2D delta = newMouse.subtract(mouse);
        mouse = newMouse;
        
        if (evt.isShiftDown()) {
            matrix.prependTranslation(delta.getX(), delta.getY());
            evt.consume();
        }
    }
    
    protected void onScroll(ScrollEvent evt) {
        if (evt.isShiftDown()) {
            Point2D pivot = new Point2D(evt.getX(), evt.getY());
            double scale = 1 - Math.signum( evt.getDeltaX() ) * 0.1;
            matrix.prependScale(scale, scale, pivot);
        }
    }
    
}
