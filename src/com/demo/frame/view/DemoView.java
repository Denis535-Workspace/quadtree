/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.frame.view;

import com.misc.voxelmap.*;
import com.misc.quadtree.*;
import com.misc.tracing.*;
import com.misc.utils.Point2i;
import com.misc.utils.Ray;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.transform.Affine;
import javafx.scene.transform.NonInvertibleTransformException;
import javax.imageio.ImageIO;

/**
 *
 * @author DDDENISSS
 */
public final class DemoView extends ScrollView {
    
    static interface AlgorithmDemo {
        
        public void paint(GraphicsContext g, VoxelMap map, Quadtree quadtree, Ray ray);
        
    }
    
    private final VoxelMap map = new VoxelMap();
    private Quadtree quadtree;
    private final Ray ray = new Ray().setPosition(-100, -100).setDirection(100, 0);
    private AlgorithmDemo demo;
    
    private float radius = 1;
    public final IntegerProperty lodProperty = new SimpleIntegerProperty();
    
    
    public DemoView() {
        super();
        setOnMouseMoved( this::onMouseMoved );
        setOnMouseEntered( v -> requestFocus() );
        setOnMouseExited( v -> paint() );
        setOnKeyPressed( this::onKeyPressed );
        setOnKeyReleased( this::onKeyReleased );
        lodProperty.addListener( v -> paint() );
        
        //matrix.prependScale(20, 20, 1);
        VoxelMapPainter.setSolid(map, 0, 0, 128, true);
        map.computeMipMaps();
    }
    
    @Override
    protected void paint() {
        GraphicsContext g = getGraphicsContext2D();
        g.setFill(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        
        g.save();
        g.setTransform(matrix);
        
        if (quadtree == null) drawGrid(g, matrix, lodProperty.get());
        VoxelMapDrawer.draw(g, map, lodProperty.get());
        if (quadtree != null) QuadtreeDrawer.draw(g, quadtree, lodProperty.get());
        if (demo != null) demo.paint(g, map, quadtree, ray);

        g.restore();
    }
    
    private static void drawGrid(GraphicsContext g, Affine matrix, int lod) {
        double scale = matrix.getMxx();
        int delta = (int) ( 10 / scale );
        delta = Math.max(delta, 1) << lod;
        
        g.setStroke(Color.RED);
        g.setLineWidth(0.1 / scale);
        
        for (int x = 0; x <= VoxelMap.SIZE; x+=delta) {
            g.strokeLine(x, 0, x, VoxelMap.SIZE);
        }
        for (int y = 0; y <= VoxelMap.SIZE; y+=delta) {
            g.strokeLine(0, y, VoxelMap.SIZE, y);
        }
    }
    
    private void drawCursor(double x, double y, boolean lightMode) {
        if (demo != null) return;
        
        GraphicsContext g = getGraphicsContext2D();
        double scale = matrix.getMxx();
        double r = radius*scale;
        
        if(lightMode) {
            g.setStroke(Color.YELLOW);
        } else {
            g.setStroke(Color.RED);
        }
        g.setLineWidth(1);
        g.strokeArc(x-r, y-r, r*2, r*2, 0, 360, ArcType.CHORD);
    }
    
    private Point2i getMapPosition() {
        try {
            Point2D pnt = matrix.inverseTransform(mouse);
            int x = (int) pnt.getX();
            int y = (int) pnt.getY();
            return new Point2i(x, y);
        } catch (NonInvertibleTransformException ex) {
            Logger.getLogger(DemoView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    protected void onMousePressed(MouseEvent evt) {
        super.onMousePressed(evt);
        
        Point2i pos = getMapPosition();

        if (demo != null && !evt.isConsumed()) { // ray
            ray.setPosition(pos.x + 0.5f, pos.y + 0.5f);
            evt.consume();
        }
        if (!evt.isControlDown() && !evt.isConsumed()) { // voxel
            if (evt.getButton() == MouseButton.PRIMARY) VoxelMapPainter.setSolid(map, pos.x, pos.y, (int) radius, true);
            if (evt.getButton() == MouseButton.SECONDARY) VoxelMapPainter.clear(map, pos.x, pos.y, (int) radius);
            map.computeMipMaps();
            if(quadtree != null) quadtree = QuadtreeBuilder.build(map);
            evt.consume();
        }
        if (evt.isControlDown() && !evt.isConsumed()) { // voxel
            if (evt.getButton() == MouseButton.PRIMARY) VoxelMapPainter.setLight(map, pos.x, pos.y, (int) radius, 1);
            if (evt.getButton() == MouseButton.SECONDARY) VoxelMapPainter.clear(map, pos.x, pos.y, (int) radius);
            map.computeMipMaps();
            if(quadtree != null) quadtree = QuadtreeBuilder.build(map);
            evt.consume();
        }

        paint();
        drawCursor(evt.getX(), evt.getY(), evt.isControlDown());
    }
 
    @Override
    protected void onMouseDragged(MouseEvent evt) {
        super.onMouseDragged(evt);
        
        Point2i pos = getMapPosition();
 
        if (demo != null && !evt.isConsumed()) { // ray
            ray.setEndPosition(pos.x + 0.5f, pos.y + 0.5f);
            evt.consume();
        }
        if (!evt.isControlDown() && !evt.isConsumed()) { // voxel
            if (evt.getButton() == MouseButton.PRIMARY) VoxelMapPainter.setSolid(map, pos.x, pos.y, (int) radius, true);
            if (evt.getButton() == MouseButton.SECONDARY) VoxelMapPainter.clear(map, pos.x, pos.y, (int) radius);
            map.computeMipMaps();
            if(quadtree != null) quadtree = QuadtreeBuilder.build(map);
            evt.consume();
        }
        if (evt.isControlDown() && !evt.isConsumed()) { // voxel
            if (evt.getButton() == MouseButton.PRIMARY) VoxelMapPainter.setLight(map, pos.x, pos.y, (int) radius, 1);
            if (evt.getButton() == MouseButton.SECONDARY) VoxelMapPainter.clear(map, pos.x, pos.y, (int) radius);
            map.computeMipMaps();
            if(quadtree != null) quadtree = QuadtreeBuilder.build(map);
            evt.consume();
        }

        paint();
        drawCursor(evt.getX(), evt.getY(), evt.isControlDown());
    }
    
    @Override
    protected void onScroll(ScrollEvent evt) {
        super.onScroll(evt);
        if (!evt.isShiftDown()) {
            float scaler = evt.getDeltaY() > 0 ? 1.1f : 0.9f;
            radius *= scaler;
            radius = Math.max(radius, 1);
            radius = Math.min(radius, 128);
        }

        paint();
        drawCursor(evt.getX(), evt.getY(), evt.isControlDown());
    }
    
    private void onMouseMoved(MouseEvent evt) {
        mouse = new Point2D( evt.getX(), evt.getY() );
        paint();
        drawCursor(evt.getX(), evt.getY(), evt.isControlDown());
    }
    
    private void onKeyPressed(KeyEvent evt) {
        if(evt.getCode() == KeyCode.F1) {
            makeSnapshot();
        }
        
        if (evt.getCode() == KeyCode.C) { // clear
            quadtree = null;
            demo = null;
        }
        
        if (evt.getCode() == KeyCode.DIGIT1) { // quadtree
            demo = null;
            quadtree = QuadtreeBuilder.build(map);
        }
        
        if (evt.getCode() == KeyCode.DIGIT2) { // top-down ray tracer
            demo = QuadtreeRayTracer::paint;
            quadtree = QuadtreeBuilder.build(map);
            
            Point2i pos = getMapPosition();
            ray.setPosition(pos.x + 0.5f, pos.y + 0.5f);
        }
        
        if (evt.getCode() == KeyCode.DIGIT3) { // Bresenham
            demo = BresenhamTracer::paint;
            quadtree = null;
            
            Point2i pos = getMapPosition();
            ray.setPosition(pos.x + 0.5f, pos.y + 0.5f);
        }
        
        if( evt.getCode() == KeyCode.DIGIT4 ) { // cone tracer
            demo = VoxelConeTracer::paint;
            quadtree = QuadtreeBuilder.build(map);
            
            Point2i pos = getMapPosition();
            ray.setPosition(pos.x + 0.5f, pos.y + 0.5f);
        }

        paint();
        drawCursor(mouse.getX(), mouse.getY(), evt.isControlDown());
    }
    
    private void onKeyReleased(KeyEvent evt) {
        paint();
        drawCursor(mouse.getX(), mouse.getY(), evt.isControlDown());
    }

    
    private void makeSnapshot() {
        paint();
        
        try {
            WritableImage image = snapshot(null, null);
            File file = File.createTempFile("image ", ".png", new File("d:/"));
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
}
