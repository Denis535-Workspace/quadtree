/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.frame;

import com.demo.frame.view.DemoView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;

/**
 *
 * @author DDDENISSS
 */
public class FrameController implements Initializable {
    
    @FXML
    private DemoView view;
    
    @FXML
    private Slider slider;
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Pane parent = (Pane) view.getParent();
        view.widthProperty().bind(parent.widthProperty());
        view.heightProperty().bind(parent.heightProperty());
        
        view.lodProperty.bind(slider.valueProperty());
    }
    
    @FXML
    private void onHelpAction() {
        String text = "Shift + mouse drag - move viewport\n"
                + "Shift + while scroll - zoom viewport\n"
                + "Mouse drag - set solid voxels\n"
                + "Ctrl + mouse drag - set light voxels\n"
                + "Mouse drag - set ray (when algorithm is running)\n"
                + "1 - build quadtree\n"
                + "2 - run ray tracer\n"
                + "3 - run Bresenham's tracer\n"
                + "4 - run cone tracer";
        
        Alert alert = new Alert(AlertType.INFORMATION);
        
        alert.setTitle("Help");
        alert.setHeaderText(null);
        alert.setContentText(text);
        
        alert.showAndWait();
    }

}
