/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.voxelmap;

import com.misc.utils.Point2i;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 *
 * @author DDDENISSS
 */
public final class VoxelMapDrawer {
    
    public static void draw(GraphicsContext g, VoxelMap map, int lod) {
        int size = VoxelMap.SIZE >> lod;
        g.save();
        g.scale(1 << lod, 1 << lod);
        
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                Cell cell = map.getCell(x, y, lod);
                fillCell(g, x, y, cell);
            }
        }
        
        g.restore();
    }
    
    private static void fillCell(GraphicsContext g, int x, int y, Cell cell) {
        if (cell.isSolid()) {
            if( cell.isLight() ) {
                Color color = Color.color(cell.light, cell.solid, 0);
                
                fillCell(g, color, x, y);
            } else {
                Color color = Color.DARKGREEN;
                color = Color.hsb(color.getHue(), color.getSaturation(), color.getBrightness() * cell.solid);
                
                fillCell(g, color, x, y);
            }
        } else {
            if (cell.isLight()) {
                Color color = Color.gray(cell.light);
                fillCell(g, color, x, y);
            }
        }
    }
  
    private static void fillCell(GraphicsContext g, Color color, int x, int y) {
        g.setFill(color);
        g.fillRect(x, y, 1, 1);
    }
    
    //public static void fillCell(GraphicsContext g, Color color, Point2i pnt, int lod) {
    //    fillCell(g, color, pnt.x, pnt.y, lod);
    //}
    
    public static void fillCell(GraphicsContext g, Color color, int x, int y, int lod) {
        Paint oldColor = g.getFill();
        int size = 1 << lod;
        x <<= lod;
        y <<= lod;
        g.setFill(color);
        g.fillRect(x, y, size, size);
        g.setFill(oldColor);
    }

}
