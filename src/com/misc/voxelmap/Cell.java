/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.voxelmap;

/**
 *
 * @author DDDENISSS
 */
public class Cell {
    
    public float solid;
    public float light;

    public void setSolid(boolean solid) {
        this.solid = solid ? 1 : 0;
    }

    public boolean isSolid() {
        return solid != 0;
    }
    
    public void setLight(float light) {
        this.light = light;
    }
    
    public boolean isLight() {
        return light != 0;
    }
    
    public void clear() {
        solid = light = 0;
    }
    
    public static boolean hasChild(float v) {
        final float E = 1f / (256*256);
        if(Math.abs(v - 0) < E) return false;
        if(Math.abs(v - 1) < E) return false;
        return true;
    }
    
}
