/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.voxelmap;

import com.sun.javafx.geom.Point2D;
import java.util.ArrayList;

/**
 *
 * @author DDDENISSS
 */
public final class VoxelMapPainter {
    
    ////////////////////////////////////////////////////////////
    public static void setSolid(VoxelMap map, int x, int y, int radius, boolean solid) {
        int[][] cells = getCells(x, y, radius);
        for(int[] pnt : cells) {
            setSolid(map, pnt[0], pnt[1], solid);
        }
    }
    
    public static void setSolid(VoxelMap map, int x, int y, boolean solid) {
        Cell cell = map.getCell(x, y, 0);
        if(cell != null && !cell.isLight()) {
            cell.setSolid(solid);
        }
    }
    
    ////////////////////////////////////////////////////////////
    public static void setLight(VoxelMap map, int x, int y, int radius, float light) {
        int[][] cells = getCells(x, y, radius);
        for(int[] pnt : cells) {
            setLight(map, pnt[0], pnt[1], light);
        }
    }
    
    public static void setLight(VoxelMap map, int x, int y, float light) {
        Cell cell = map.getCell(x, y, 0);
        if (cell != null && !cell.isSolid()) {
            cell.setLight(light);
        }
    }
    
    ////////////////////////////////////////////////////////////
    public static void clear(VoxelMap map, int x, int y, int radius) {
        int[][] cells = getCells(x, y, radius);
        for(int[] pnt : cells) {
            clear(map, pnt[0], pnt[1]);
        }
    }
    
    public static void clear(VoxelMap map, int x, int y) {
        Cell cell = map.getCell(x, y, 0);
        if (cell != null) cell.clear();
    }
    
    ////////////////////////////////////////////////////////////
    private static int[][] getCells(int px, int py, int radius) {
        ArrayList<int[]> list = new ArrayList();
        for (int y = -radius; y <= radius; y++) {
            for (int x = -radius; x <= radius; x++) {
                if( Point2D.distanceSq(0, 0, x, y) < radius*radius ) {
                    list.add( new int[] {px+x, py+y} );
                }
            }
        }
        return list.toArray( new int[0][] );
    }
    
    ////////////////////////////////////////////////////////////
    public static void clear(VoxelMap map) {
        for(Cell[] layer : map.maps)
            for(Cell cell : layer)
                cell.clear();
    }
    
}
