/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.voxelmap;

/**
 *
 * @author DDDENISSS
 */
public class VoxelMap {
    
    public static final int SIZE = 256;
    public final Cell[][] maps = new Cell[8][]; // from 256*256 to 2*2

    public VoxelMap() {
        for (int lod = 0; lod < maps.length; lod++) {
            int size = VoxelMap.SIZE >> lod;
            maps[lod] = new Cell[size * size];
            for (int j = 0; j < maps[lod].length; j++) {
                maps[lod][j] = new Cell();
            }
        }
    }
    
    
    public void computeMipMaps() {
        for (int lod = 1; lod < maps.length; lod++) {
            computeMap(lod);
        }
    }
    
    private void computeMap(final int lod) {
        int size = getMapSizeByLOD(lod);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                Cell cell = getCell(x, y, lod);
                
                Cell child_1 = getCell(x*2+0, y*2+0, lod-1);
                Cell child_2 = getCell(x*2+1, y*2+0, lod-1);
                Cell child_3 = getCell(x*2+0, y*2+1, lod-1);
                Cell child_4 = getCell(x*2+1, y*2+1, lod-1);
                
                cell.solid = (child_1.solid + child_2.solid + child_3.solid + child_4.solid) / 4f;
                cell.light = (child_1.light + child_2.light + child_3.light + child_4.light) / 4f;
            }
        }
    }
    
    
    public Cell getCell(int x, int y, int lod) {
        if(check(x, y, lod)) return maps[lod][ toIndex(x, y, lod) ];
        return null;
    }
    
    public static boolean check(int x, int y, int lod) {
        int size = getMapSizeByLOD(lod);
        return x >= 0 && x < size && 
               y >= 0 && y < size;
    }
    
    public static int toIndex(int x, int y, int lod) {
        int size = getMapSizeByLOD(lod);
        return y * size + x;
    }
    
    
    public static int getMapSizeByLOD(int lod) { // 0 lod == 256 size
        return VoxelMap.SIZE >> lod;
    }
    
    public static int getCellSizeByLayer(int layer) { // 0 layer == 256
        return VoxelMap.SIZE >> layer;
    }
    
}
