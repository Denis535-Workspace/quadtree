/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.tracing;

import com.misc.quadtree.Quadtree;
import com.misc.utils.Ray;
import com.misc.voxelmap.VoxelMap;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 *
 * @author DDDENISSS
 */
public class VoxelConeTracer {
    
    private static final float RADIUS = 64;
    private static final int CONE_COUNT = 4;
    
    public static void paint(GraphicsContext g, VoxelMap map, Quadtree quadtree, Ray ray) {
        float rot = (float) Math.atan2(ray.dirY, ray.dirX);
        rot = (float) Math.toDegrees(rot) - 90;
        
        g.setStroke(Color.YELLOW);
        g.strokeArc(ray.x-RADIUS, ray.y-RADIUS, RADIUS*2, RADIUS*2, -rot, -180, ArcType.OPEN);
        
        for (int i = 0; i <= CONE_COUNT; i++) {
            float tRot = rot + i * 180f / CONE_COUNT;
            float dirX = (float) Math.cos( Math.toRadians(tRot) ) * RADIUS;
            float dirY = (float) Math.sin( Math.toRadians(tRot) ) * RADIUS;
            
            g.strokeLine(ray.x, ray.y, ray.x + dirX, ray.y + dirY);
        }
    }
    
}
