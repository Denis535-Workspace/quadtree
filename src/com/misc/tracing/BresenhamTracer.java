/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.tracing;

import com.misc.quadtree.Quadtree;
import com.misc.utils.Point2i;
import com.misc.utils.Ray;
import com.misc.voxelmap.Cell;
import com.misc.voxelmap.VoxelMap;
import java.util.ArrayList;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author DDDENISSS
 */
public class BresenhamTracer {
    
    private static final Point2i[] rays;
    
    static {
        ArrayList<Point2i> list = new ArrayList();
        for (int y = 0; y < 16; y++) {
            list.add(new Point2i(-16, y) ); // left column
            list.add(new Point2i( 16, y) ); // right column
        }
        for (int x = -16; x <= 16; x++) {
            list.add(new Point2i( x, 16) );
        }
        rays = list.toArray(new Point2i[0] ); // hemicube
    }
    
    public static void paint(GraphicsContext g, VoxelMap map, Quadtree quadtree, Ray ray) {
        /*int x = (int) ray.x;
        int y = (int) ray.y;
        
        g.setFill(Color.RED);
        g.fillArc(x, y, 1, 1, 0, 360, ArcType.CHORD);
        
        g.setStroke(Color.YELLOW);
        for(Point2i dir : rays) {
            int x2 = x + dir.x;
            int y2 = y + dir.y;
            Point2i hit = lineCast(map, x, y, x2, y2);
            if( hit != null ) {
                g.setStroke(Color.RED);
            } else {
                g.setStroke(Color.YELLOW);
            }
            g.strokeLine(x+0.5, y+0.5, x2+0.5, y2+0.5);
        }*/
        
        
        int x = (int) ray.x;
        int y = (int) ray.y;
        
        int x2 = (int) ray.getEndPoint().getX();
        int y2 = (int) ray.getEndPoint().getY();
        
        Point2i hit = lineCast( map, x, y, x2, y2 );
        if( hit != null ) {
            ray.paint(g, Color.RED);
        } else {
            ray.paint(g, Color.YELLOW);
        }
    }
    
    private static Point2i lineCast(VoxelMap map, int x, int y, int x2, int y2) { // Bresenham
        int deltaX = Math.abs(x2 - x);
        int deltaY = Math.abs(y2 - y);

        int stepX = sign(x2 - x);
        int stepY = sign(y2 - y);
        
        
        if (deltaX > deltaY) { // h
            int error = 0;
            while ( true ) {
                //VoxelMapDrawer.fillCell(g, Color.gray(1, 0.3), x, y, 0);
                if( isSolid(map, x, y) ) return new Point2i(x, y);

                error += deltaY;
                if (error * 2 >= deltaX) {
                    y += stepY;
                    error -= deltaX;
                    //VoxelMapDrawer.fillCell(g, Color.gray(1, 0.3), x, y, 0);
                    if( isSolid(map, x, y) ) return new Point2i(x, y);
                }
                
                
                if(x == x2) break;
                x += stepX;
            }
        } else { // v
            int error = 0;
            while ( true ) {
                //VoxelMapDrawer.fillCell(g, Color.gray(1, 0.3), x, y, 0);
                if( isSolid(map, x, y) ) return new Point2i(x, y);

                error += deltaX;
                if (error * 2 >= deltaY) {
                    x += stepX;
                    error -= deltaY;
                    //VoxelMapDrawer.fillCell(g, Color.gray(1, 0.3), x, y, 0);
                    if( isSolid(map, x, y) ) return new Point2i(x, y);
                }
                
                
                if(y == y2) break;
                y += stepY;
            }
            
        }

        return null;
    }
    
    private static boolean isSolid(VoxelMap map, int x, int y) {
        Cell cell = map.getCell(x, y, 0);
        return cell != null && cell.isSolid();
    }
    
    private static int sign(float x) {
        return x < 0 ? -1 : 1;
    }
    
}
