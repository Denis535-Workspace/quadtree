/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.tracing;

import com.misc.quadtree.Leaf;
import com.misc.quadtree.Node;
import com.misc.quadtree.Quadtree;
import com.misc.utils.Point2i;
import com.misc.utils.Ray;
import com.misc.voxelmap.VoxelMap;
import com.misc.voxelmap.VoxelMapDrawer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author DDDENISSS
 */
public class QuadtreeRayTracer {
    
    public static final class RayHit {
        
        public final int x, y, layer;

        public RayHit(int x, int y, int layer) {
            this.x = x;
            this.y = y;
            this.layer = layer;
        }
        
    }
    
    
    public static void paint(GraphicsContext g, VoxelMap map, Quadtree quadtree, Ray ray) {
        RayHit hit = raycast(quadtree, ray);
        if(hit != null) {
            Color color = new Color(1, 0, 0, 0.3);
            VoxelMapDrawer.fillCell(g, color, hit.x, hit.y, Quadtree.layerToLOD(hit.layer));
            
            ray.paint(g, Color.RED);
        } else {
            ray.paint(g, Color.YELLOW);
        }
    }
    
    
    private static RayHit raycast(Quadtree quadtree, Ray ray) {
        ray = ray.clone();
        
        int indexTransform = 0;
        if (ray.dirX < 0) {
            ray.x = VoxelMap.SIZE - ray.x;
            ray.dirX = -ray.dirX;
            indexTransform |= 1;
        }
        if (ray.dirY < 0) {
            ray.y = VoxelMap.SIZE - ray.y;
            ray.dirY = -ray.dirY;
            indexTransform |= 2;
        }
        
        float x0 = (0 - ray.x) / ray.dirX;
        float x2 = (VoxelMap.SIZE - ray.x) / ray.dirX;

        float y0 = (0 - ray.y) / ray.dirY;
        float y2 = (VoxelMap.SIZE - ray.y) / ray.dirY;
        
        if ( Math.max(x0, y0) < Math.min(x2, y2) ) {
            return raycast(quadtree, ray, indexTransform, 
                    quadtree.getRoot(), 0, 0, 0, 
                    x0, y0, x2, y2 );
        }
        return null;
    }
    
    private static RayHit raycast(Quadtree quadtree, Ray ray, int indexTransform,
                        Node node, int nodeX, int nodeY, int layer,
                        float x0, float y0, float x2, float y2) {
        
        float x1 = (x0 + x2) / 2; // center
	float y1 = (y0 + y2) / 2;
        
        if( Float.isNaN(x1) ) {
            int size = VoxelMap.getCellSizeByLayer(layer);
            int nodeCenterX = (nodeX * size) + size/2;
            x1 = ray.x > nodeCenterX ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
        }
        if( Float.isNaN(y1) ) {
            int size = VoxelMap.getCellSizeByLayer(layer);
            int nodeCenterY = (nodeY * size) + size/2;
            y1 = ray.y > nodeCenterY ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
        }
        
        int result = getCrossedNodes(x0, y0,
                                     x1, y1,
                                     x2, y2);
        
        int index = node.childTileIndex;
        
        final float[] xList = new float[] {x0, x1, x2};
        final float[] yList = new float[] {y0, y1, y2};
        
        layer++;
        for (int i = 0; i < 4; i++) {
            if ((result & (1 << i)) != 0) {
                Point2i delta = Quadtree.delta[i];
                float cx0 = xList[delta.x];
                float cy0 = yList[delta.y];
                float cx2 = xList[delta.x+1];
                float cy2 = yList[delta.y+1];
                
                if(cx2 < 0 || cy2 < 0) continue; // саб-нод сзади луча
                if(cx0 > ray.length || cy0 > ray.length) continue; // луч не достает до саб-нода
                
                int i2 = i ^ indexTransform;
                delta = Quadtree.delta[i2];
                int childX = 2 * nodeX + delta.x;
                int childY = 2 * nodeY + delta.y;
                
                if (node.hasChild(i2)) {
                    Node child = quadtree.getNode(index + i2);
                    RayHit hit = raycast(quadtree, ray, indexTransform,
                                          child, childX, childY, layer,
                                          cx0, cy0, cx2, cy2);
                    if(hit != null) return hit;
                } else {
                    Leaf leaf = quadtree.getLeaf(index + i2);
                    if(leaf.value) return new RayHit(childX, childY, layer);
                }

            }
        }
        
        return null;
    }
    
    private static int getCrossedNodes(float x0, float y0, 
                                       float x1, float y1,
                                       float x2, float y2) {
        
        //0\  1
        //  \
        //2  \3
        
        int mask = 0;
        if(y1 > x1) { // top the diagonal
            // crossed with 1
            mask |= 1 << 1;
            if(y0 < x1) mask |= 1 << 0; // crossed with 0
            if(y1 < x2) mask |= 1 << 3; // crossed with 3
        } else { // below the diagonal
            // crossed with 2
            mask |= 1 << 2;
            if(x0 < y1) mask |= 1 << 0; // crossed with 0
            if(x1 < y2) mask |= 1 << 3; // crossed with 3
        }
        
        return mask;
    }
    
}
