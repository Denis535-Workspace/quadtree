/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.utils;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author DDDENISSS
 */
public final class Ray {
    
    public float x, y;
    public float dirX, dirY;
    public float length;

    public Ray() {
    }
    
    public Ray(float x, float y, float dirX, float dirY, float length) {
        this.x = x;
        this.y = y;
        this.dirX = dirX;
        this.dirY = dirY;
        this.length = length;
    }
    
    public void paint(GraphicsContext g, Color color) {
        float x2 = x + dirX * length;
        float y2 = y + dirY * length;
        
        g.setStroke(color);
        g.setFill(color);
        g.setLineWidth(1f / g.getTransform().getMxx());
        g.fillOval(x-0.5, y-0.5, 1, 1);
        g.strokeLine(x, y, x2, y2);
    }
    
    public Ray setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }
    
    public Ray setEndPosition(float x, float y) {
        return setDirection( x - this.x, y - this.y );
    }

    public Ray setDirection(float dirX, float dirY) {
        this.length = (float) Math.sqrt( dirX*dirX + dirY*dirY );
        this.dirX = dirX / length;
        this.dirY = dirY / length;
        return this;
    }
    
    public Point2D getPoint(float dis) {
        return new Point2D(x + dirX * dis, y + dirY * dis);
    }
    
    public Point2D getEndPoint() {
        return getPoint( length );
    }
    
    
    @Override
    public Ray clone() {
        return new Ray(x, y, dirX, dirY, length);
    }
    
}
