/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.utils;

/**
 *
 * @author DDDENISSS
 */
public class Point3i {
    
    public int x, y, z;
    
    public Point3i() {
    }
    
    public Point3i(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
}
