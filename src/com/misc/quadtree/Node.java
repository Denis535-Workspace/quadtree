/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.quadtree;

/**
 *
 * @author DDDENISSS
 */
public class Node {

    public int children;
    public int childTileIndex;
    

    public void setChild(int i) {
        children |= 1 << i;
    }
    
    public boolean hasChild(int i) {
        int mask = 1 << i;
        return (children & mask) != 0;
    }
    
    
    @Override
    public String toString() {
        int c1 = hasChild(0) ? 1 : 0;
        int c2 = hasChild(1) ? 1 : 0;
        int c3 = hasChild(2) ? 1 : 0;
        int c4 = hasChild(3) ? 1 : 0;
        
        return String.format("Node (%d %d %d %d)", c1, c2, c3, c4);
    }

}
