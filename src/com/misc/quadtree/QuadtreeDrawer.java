/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.quadtree;

import com.misc.utils.Point2i;
import com.misc.voxelmap.VoxelMap;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author DDDENISSS
 */
public final class QuadtreeDrawer {
    
    public static void draw(GraphicsContext g, Quadtree tree, int minLOD) {
        g.setStroke(Color.RED);
        g.setLineWidth(1 / g.getTransform().getMxx());
        
        g.strokeRect(0, 0, VoxelMap.SIZE, VoxelMap.SIZE);
        draw(g, tree, tree.getRoot(), 0, 0, 0, Quadtree.lodToLayer(minLOD));
    }
    
    public static void draw(GraphicsContext g, Quadtree tree, Node node, int x, int y, int layer, int maxLayer) {
        if(node.children != 0 && layer < maxLayer) drawCross(g, x, y, layer);
        
        int index = node.childTileIndex;
        layer++;
        for (int i = 0; i < 4; i++, index++) {
            if (node.hasChild(i)) {
                Node child = tree.getNode(index);
                Point2i delta = Quadtree.delta[i];
                int cx = 2*x+delta.x; // child pos
                int cy = 2*y+delta.y;
                draw(g, tree, child, cx, cy, layer, maxLayer);
            } else {
                Leaf leaf = tree.getLeaf(index);
            }
        }
        
    }
    
    
    private static void drawCross(GraphicsContext g, int x, int y, int layer) {
        int size = VoxelMap.getCellSizeByLayer(layer);
        int halfSize = size / 2;
        x = x * size + halfSize;
        y = y * size + halfSize;
        
        g.strokeLine(x-halfSize, y, x+halfSize, y);
        g.strokeLine(x, y-halfSize, x, y+halfSize);
    }
    
}
