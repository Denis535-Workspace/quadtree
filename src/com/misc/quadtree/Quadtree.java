/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.quadtree;

import com.misc.utils.Point2i;

/**
 *
 * @author DDDENISSS
 */
public class Quadtree {
    
    public static final Point2i[] delta = new Point2i[] { // sub-nodes positions
        new Point2i(0, 0),
        new Point2i(1, 0),
        new Point2i(0, 1),
        new Point2i(1, 1),
    };
    
    private final Object[] nodes; // nodes and leaves

    public Quadtree(Object[] nodes) {
        this.nodes = nodes;
    }
    
    public Node getRoot() {
        return getNode(0);
    }
    
    public Node getNode(int index) {
        return (Node) nodes[index];
    }
    
    public Leaf getLeaf(int index) {
        return (Leaf) nodes[index];
    }
    
    
    public static int lodToLayer(int lod) {
        return 8 - lod;
    }
    
    public static int layerToLOD(int layer) {
        return 8 - layer;
    }
    
}
