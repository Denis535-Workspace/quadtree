/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.quadtree;

import com.misc.voxelmap.Cell;
import com.misc.voxelmap.VoxelMap;
import java.util.ArrayList;

/**
 *
 * @author DDDENISSS
 */
public final class QuadtreeBuilder {
    
    public interface CellFactory {
        public float getValue(VoxelMap map, int x, int y, int lod);
    }
    
    static class NodePos {
        int x, y;
        
        public NodePos(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    
    public static final CellFactory solidFactory = (map, x, y, lod) -> map.getCell(x, y, lod).solid;
    public static final CellFactory emptyFactory = (map, x, y, lod) -> 1-map.getCell(x, y, lod).solid;
    public static final CellFactory lightFactory = (map, x, y, lod) -> map.getCell(x, y, lod).light;
    
    
    public static Quadtree build(VoxelMap map) {
        return build( map, solidFactory );
    }
    
    public static Quadtree build(VoxelMap map, CellFactory factory) {
        ArrayList nodes = buildTree( map, factory );
        return new Quadtree( nodes.toArray() );
    }
    
    
    private static ArrayList buildTree(VoxelMap map, CellFactory factory) {
        ArrayList nodes = new ArrayList();
        ArrayList<NodePos> nodePositions = new ArrayList();
        
        nodes.add(new Node()); // root
        nodePositions.add(new NodePos(0, 0));

        int index = 0;
        for (int lod = 8; lod > 0; lod--) {
            index = buildLayer( map, factory, nodes, nodePositions, index, lod );
        }
        
        return nodes;
    }
    
    
    private static int buildLayer(VoxelMap map, CellFactory factory, ArrayList nodes, ArrayList<NodePos> nodePositions, int index, int lod) {
        int end = nodes.size();
        for (; index < end; index++) {
            Object obj = nodes.get(index);
            NodePos pos = nodePositions.get(index);
            if (obj instanceof Node) {
                Node node = (Node) obj;
                node.childTileIndex = nodes.size();
                buildChildTile(map, factory, nodes, nodePositions, node, pos, lod);
            }
        }
        return index;
    }
    
    private static void buildChildTile(VoxelMap map, CellFactory factory, ArrayList nodes, ArrayList<NodePos> nodePositions, Node node, NodePos pos, int lod) {
        int tileX = pos.x * 2;
        int tileY = pos.y * 2;
        lod--;
        
        for (int i = 0; i < 4; i++) {
            int cx = tileX + Quadtree.delta[i].x;
            int cy = tileY + Quadtree.delta[i].y;
            
            Object child = createNode(factory, map, cx, cy, lod);
            if (child instanceof Node) node.setChild(i);
            nodes.add(child);
            nodePositions.add( new NodePos(cx, cy) );
        }
    }
    
    
    private static Object createNode(CellFactory factory, VoxelMap map, int x, int y, int lod) {
        float v = factory.getValue(map, x, y, lod);
        if (Cell.hasChild(v)) return new Node();
        return new Leaf(v > 0.5f);
    }
    
}
