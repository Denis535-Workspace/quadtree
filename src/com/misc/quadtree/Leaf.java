/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.quadtree;

/**
 *
 * @author DDDENISSS
 */
public class Leaf {
    
    public boolean value;

    public Leaf(boolean value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return "Leaf "+value;
    }
}
