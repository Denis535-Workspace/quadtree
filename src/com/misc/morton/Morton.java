/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misc.morton;

import com.misc.utils.Point3i;
import com.misc.voxelmap.Cell;
import com.misc.voxelmap.VoxelMap;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.transform.Affine;

/**
 *
 * @author DDDENISSS
 */
public class Morton {
    
    
    public static int[] computeMortonList(VoxelMap map) {
        ArrayList<Integer> list = new ArrayList();
        
        for (int lod = 7; lod >= 0; lod--) {
            int texSize = VoxelMap.SIZE >> lod;
            int cellSize = 1 << lod;
            
            for (int y = 0; y < texSize; y++) {
                for (int x = 0; x < texSize; x++) {
                    boolean parentHasChildren = true;
                    if (lod != 7) {
                        Cell parentCell = map.getCell(x / 2, y / 2, lod + 1);
                        parentHasChildren = Cell.hasChild(parentCell.solid);
                    }
                    
                    Cell cell = map.getCell(x, y, lod);
                    if( parentHasChildren && !Cell.hasChild(cell.solid) && cell.solid > 0.5f ) {
                        int cx = (x << lod) + cellSize/2;
                        int cy = (y << lod) + cellSize/2;
                        list.add( encode(cx, cy, 0) );
                    }
                }
            }
            
        }
        
        int[] array = new int[ list.size() ];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        
        Arrays.sort( array );
        return array;
    }
    
    
    public static void drawMortonList(GraphicsContext g, Affine matrix, int[] list) {
        double scale = matrix.getMxx();
        g.setStroke(Color.YELLOW);
        g.setFill(Color.YELLOW);
        g.setLineWidth(1 / scale);
        
        Point3i prevPnt = null;
        for (int i : list) {
            Point3i pnt = Morton.decode(i);
            if( prevPnt != null ) {
                g.strokeLine(prevPnt.x+0.5, prevPnt.y+0.5, pnt.x+0.5, pnt.y+0.5);
            }
            g.fillArc(pnt.x+0.1, pnt.y+0.1, 0.8, 0.8, 0, 360, ArcType.OPEN);
            prevPnt = pnt;
        }
    }
    
    
    public static int encode(int x, int y, int z){
        int answer = 0;
        for (int i = 0; i < 64; i++) {
            int t0 = 2 * i + 0;
            int t1 = 2 * i + 1;
            int t2 = 2 * i + 2;
            int mask = 1 << i;
            
            answer |= (x & mask) << t0;
            answer |= (y & mask) << t1;
            answer |= (z & mask) << t2;
        }
        return answer;
    }
 
    public static Point3i decode(int morton) {
        int x = 0;
        int y = 0;
        int z = 0;
     
        for (int i = 0; i < 64 / 3; i++) {
            int t0 = 3 * i + 0;
            int t1 = 3 * i + 1;
            int t2 = 3 * i + 2;
            
            x |= (morton & (1l << t0)) >> (t0 - i);
            y |= (morton & (1l << t1)) >> (t1 - i);
            z |= (morton & (1l << t2)) >> (t2 - i);
        }
        
        return new Point3i(x, y, z);
    }
    
}
